	/*
		@author Matīss Zērvēns
		@dateCreated 05.03.2015
	*/	
	.text
	.align	2
	.global	qs
	.type	qs, %function


/*
	Function implements simple array partitioning algorithm 
	@parameters:
	  r0 integer array start address
	  r1 left-most array index to be partitioned
	  r2 right-most array index to be partitioned
  */
partition:
	sub sp, sp, #8
	str lr, [sp]

	sub sp, sp, #8
	str r4, [sp]

	sub sp, sp, #8
	str r5, [sp]

	sub sp, sp, #8
	str r6, [sp]

	sub sp, sp, #8
	str r7, [sp]

	sub sp, sp, #8
	str r8, [sp]

	mov r4, #4
	mul r4, r4, r2
	add r4, r0

	/*Loads the pivot element*/
	ldr r3, [r4]

	/*Store index*/
	mov r4, r1

	/*Store index address*/
	mov r7, #4
	mul r7, r7, r4
	add r7, r7, r0

	/*Iteration index*/
	mov r5, r4

	/*Iteration index address*/
	mov r6, r7

	/*Cycle last index r2*/

	while:
	cmp r5, r2
	bge partition_end

	ldr r1, [r6]

	cmp r1, r3
	ldrlt r8, [r7]
	strlt r1, [r7]
	strlt r8, [r6]



	addlt r4, r4, #1
	addlt r7, r7, #4
	add r5, r5, #1
	add r6, r6, #4
	b while


partition_end:
	ldr r0, [r7]
	str r3, [r7]
	str r0, [r6]

	mov r0, r4

	/* Restore registers and exit function */
	ldr r8, [sp]
	add sp, sp, #8

	ldr r7, [sp]
	add sp, sp, #8

	ldr r6, [sp]
	add sp, sp, #8

	ldr r5, [sp]
	add sp, sp, #8

	ldr r4, [sp]
	add sp, sp, #8

	ldr lr, [sp]
	add sp, sp, #8

bx lr




/*
	Quicksort algorithm implementation in ARM assembly
	@parameters:
	  r0 integer array start address
	  r1 left-most array index to be sorted
	  r2 right-most array index to be sorted
  */
qs:

	cmp r1, r2
	bge end

	/*Stores all register values on the stack*/
	sub sp, sp, #8
	str r4, [sp]

	sub sp, sp, #8
	str r5, [sp]

	sub sp, sp, #8
	str r6, [sp]

	sub sp, sp, #8
	str r7, [sp]

	sub sp, sp, #8
	str lr, [sp]

	/*Save the function parameters from being overwritten by other function calls*/
	mov r4, r0
	mov r5, r1
	mov r6, r2

	bl partition

	/*Store the pivot index*/
	mov r7, r0

	/*Set parameters for next 2 recursion calls*/
	mov r2, r0
	sub r2, r2, #1
	mov r0, r4
	mov r1, r5
	bl qs

	mov r1, r7
	add r1, r1, #1
	mov r0, r4
	mov r2, r6
	bl qs


	/*Restore register values and stack pointer*/
	ldr lr, [sp]
	add sp, sp, #8

	ldr r7, [sp]
	add sp, sp, #8

	ldr r6, [sp]
	add sp, sp, #8

	ldr r5, [sp]
	add sp, sp, #8

	ldr r4, [sp]
	add sp, sp, #8 

	/*Return control to the callee*/
end:
	bx lr
