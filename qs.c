#include <stdio.h>
#include <stdlib.h>

int qs(int * arr, int start, int end);

int main ()
{
	int array[] = {3, -1, 0, 7, 2, -5, 8, 2};
	int start = 0;
	int end = 7;
	
	qs(array, start, end);
	
	for(int i = 0; i <= end; i++)
	{
		printf("%d ", array[i]);
	}
	printf("\n");
	
	return 0;
}
